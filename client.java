package client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.security.cert.CertificateException;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

/**
 *
 * @author Lais Morais Gatti
 */
public class Client {
    
    //Connection variables
    public static final String HOSTNAME = "devdeb.legendaarne.eu";
    public static final int PORT = 22800;
    public static final String API_KEY = "lamg";
    
    public static void main(String[] args) throws IOException, CertificateException {
                
        try{
            //Connect to Sync Server
            SSLSocketFactory ssf = (SSLSocketFactory) SSLSocketFactory.getDefault();
            SSLSocket s = (SSLSocket) ssf.createSocket(HOSTNAME, PORT); 
                                 
            //Authentication
            SSLParameters params = s.getSSLParameters();
            params.setEndpointIdentificationAlgorithm(API_KEY);                      
            s.setSSLParameters(params);
                     
            //Testing the connection
            if (s.isConnected()) {
                System.out.println("Connected");
            }
            else{
                System.out.println("Not connected");
            }                             
            
            //Server return
            InputStream inputstream = System.in;
            InputStreamReader inputstreamreader = new InputStreamReader(inputstream);
            BufferedReader bufferedreader = new BufferedReader(inputstreamreader);

            OutputStream outputstream = s.getOutputStream();
            OutputStreamWriter outputstreamwriter = new OutputStreamWriter(outputstream);
            BufferedWriter bufferedwriter = new BufferedWriter(outputstreamwriter);

            String out = null;
            
            //It is not working.
            //The bufferedreader.readLine is not responding.
            while ((out = bufferedreader.readLine()) != null) {
                bufferedwriter.write(out + '\n');
                bufferedwriter.flush();
                bufferedwriter.close();
            }            
                                                  
            //Succesfully connected
            System.out.println("Connected " +
              HOSTNAME + ":" + PORT);
           
            //Printing server return
            System.out.println(out);
           
            //Close the connection
            s.close();
           
        }catch (IOException ioe) {
           //Unsuccesfull connection 
           System.err.println(ioe);
           System.err.println("Can not establish connection to " +
               HOSTNAME + ":" + PORT);           
           System.exit(-1);
        }        
    }    
}
